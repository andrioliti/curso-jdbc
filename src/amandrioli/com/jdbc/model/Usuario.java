package amandrioli.com.jdbc.model;

//essa é uma classe javabean
public class Usuario {

	private Integer codigo;
	private String nome;
	private String sobrenome;
	private Integer idade;

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public Integer getCodigo() {
		return this.codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public Integer getIdade() {
		return idade;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}

	@Override
	public String toString() {
		return this.codigo + " - " + this.nome + " - "  + this.sobrenome + " - " + this.idade;
	}

}
