package amandrioli.com.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import amandrioli.com.jdbc.model.Usuario;

public class Main {

	public static void main(String... args) throws SQLException {
		Connection con = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/postgres?user=curso&password=12345");
		PreparedStatement stm = con.prepareStatement("SELECT * FROM usuario");
		ResultSet res = stm.executeQuery();
		
		while (res.next()) {
			Usuario usuario = new Usuario();
			
			usuario.setCodigo(res.getInt("codigo"));
			usuario.setNome(res.getString("nome") );
			usuario.setSobrenome(res.getString("sobrenome") );
			usuario.setIdade(res.getInt("idade"));

			System.out.println(usuario.getNome().toUpperCase());
		}
		
		con.close();
	}

}
